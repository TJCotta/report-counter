
// Import statements
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.Writer;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileOwnerAttributeView;
import java.nio.file.attribute.UserPrincipal;
import java.nio.file.attribute.FileTime;
import java.nio.file.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.Arrays;
/**
 * @author Medic-Lap
 */
@SuppressWarnings("unused")
public class ReportCounts implements FileOwnerAttributeView{
    static String filePath[] = new String[10];
    static String pairList[][] = new String[500][3];

    static String user1 = "";
    static int user1Count = 0;
    static String user2 = "";
    static int user2Count = 0;
    static String user3 = "";
    static int user3Count = 0;
    static String user4 = "";
    static int user4Count = 0;
    static String user5 = "";
    static int user5Count = 0;
    static String user6 = "";
    static int user6Count = 0;
    static long searchDate;
    
    public static void main(String[] args) {

        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        int temp = 0;
        createPath();
        // Read config file for previous mappings.
        readConfigs();
        
        while (true){
            temp = printMenu();
            switch (temp){
                case 1: {
                    System.out.println();
                    System.out.println("Please enter the file path to add:");

                    try {
	                    String path = input.readLine();
	                    
	                    if (filePath[0] == null ){
	                        filePath[0] = path;
	                        // write file paths to a config file
	                        writeToFile(1);
	                    }
	                    else if (filePath[1] == null){
	                        filePath[1] = path;
	                        writeToFile(1);
	                    }
	                    else if (filePath[2] == null){
	                        filePath[2] = path;
	                        writeToFile(1);
	                    }
	                    else if (filePath[3] == null){
	                        filePath[3] = path;
	                        writeToFile(1);
	                    }
	                    else if (filePath[4] == null){
	                        filePath[4] = path;
	                        writeToFile(1);
	                    }
	                    else if (filePath[5] == null){
	                        filePath[5] = path;
	                        writeToFile(1);
	                    }
	                    else if (filePath[6] == null){
	                        filePath[6] = path;
	                        writeToFile(1);
	                    }
	                    else if (filePath[7] == null){
	                        filePath[7] = path;
	                        writeToFile(1);
	                    }
	                    else if (filePath[8] == null){
	                        filePath[8] = path;
	                        writeToFile(1);
	                    }
	                    else if (filePath[9] == null){
	                        filePath[9] = path;
	                        writeToFile(1);
	                    }
	                    else {
	                        System.out.println("All file paths are full. \nPlease "
	                                + "try deleting a file path before adding more.");
	                    }

            		} catch (IOException e) {
            			System.out.println("something went wrong reading the file path");
            			e.printStackTrace();
            		}
                    
                }
                break;
                // Print the file paths
                case 2: {
                    System.out.println();
                    for (int i=0; i<10; i++)
                        System.out.println(i + ". " + filePath[i]);
                }
                break;
                // Delete the file path
                case 3: {
                    System.out.println();
                    System.out.println("Enter the path number to delete.");
                    for (int i=0; i<10; i++)
                        System.out.println(i + ". " + filePath[i]);
                    int del = 100;

                    
                    try {
                    del = Integer.parseInt(input.readLine());
                    filePath[del] = null;
                    writeToFile(2);
                    } catch (NumberFormatException e) {
            			// TODO Auto-generated catch block
            			e.printStackTrace();
            		} catch (IOException e) {
            			// TODO Auto-generated catch block
            			e.printStackTrace();
            		}
                }
                break;
                // Run the counts
                case 4: {
                    System.out.println();
                    System.out.println("Please enter the search date. mm/dd/yyyy");
                    String date = "";
                    try {
                    	String date1 = input.readLine();
                    	date = date1;
                    }catch (NumberFormatException e) {
            			// TODO Auto-generated catch block
            			e.printStackTrace();
            		} catch (IOException e) {
            			// TODO Auto-generated catch block
            			e.printStackTrace();
            		}
                    try {
                        searchDate = dateConverter(date);
                    } 
                    catch (ParseException ex) {
                    	// Catches malformed date entries
                        Logger.getLogger(ReportCounts.class.getName()).log(Level.SEVERE, null, ex);
                    }
                      
                    for (String path : filePath){
                        if (!path.equals("null")){
                    		listFiles(path);
         // This seems disjointed some how.*(***********************************************************************************************           		
	                        // List to store each owner for each file
                    		ArrayList<String> users = new ArrayList<>();
	                       
	                        // Add all the owners to a list
	                        for (int i=0; i<500; i++){
	                            users.add( pairList[i][1]);
	                        }

	                        // Remove the duplicates
	                        ArrayList<String> owners = new ArrayList<>();
	                        owners = removeDuplicates(users);
	                        String[] owner = owners.toArray(new String[owners.size()]);
	                        // Set all owners to testable variables
	                        if (owners.size() > 0 && owner[0] != null)
	                            user1 = owner[0];
	                        if (owners.size() > 1 && owner[1] != null)
	                            user2 = owner[1];
	                        if (owners.size() > 2 && owner[2] != null)
	                            user3 = owner[2];
	                        if (owners.size() > 3 && owner[3] != null)
	                            user4 = owner[3];
	                        if (owners.size() > 4 && owner[4] != null)
	                            user5 = owner[4];
	                        if (owners.size() > 5 && owner[5] != null)
	                            user6 = owner[5];
	                        	                        
	                        // Set up the arrays for calculating averages and medians per user
	                        long user1FileList[] = new long[500];
	                        long user2FileList[] = new long[500];
	                        long user3FileList[] = new long[500];
	                        long user4FileList[] = new long[500];
	                        long user5FileList[] = new long[500];
	                        long user6FileList[] = new long[500];
	                        
	                        // Count the number of files created by each owner	
	                        for (int i=0; i<500; i++){
	                            if( user1.equals(pairList[i][1]) ){
	                                user1Count++;
	                                try {
	                                	// Create a separate array to calculate median and averages
										user1FileList[user1Count - 1] = milliDateConverter(pairList[i][2]);
									} catch (ParseException e) {
										System.out.println("It looks like this file" + pairList[i][0] + "doesn't have a proper creation date... hmmm"); 
									}        	
	                            }
	                            
	                            else if( user2.equals(pairList[i][1]) && user2 != "null"){  /// ************************************************************ could be the != "null" maybe try != null
	                                user2Count++;
	                                try {
	                                	// Create a separate array to calculate median and averages
										user2FileList[user2Count - 1] = milliDateConverter(pairList[i][2]);
									} catch (ParseException e) {
										System.out.println("It looks like this file" + pairList[i][0] + "doesn't have a proper creation date... hmmm"); 
									}
	                            }
	                            
	                            else if( user3.equals(pairList[i][1]) && user3 != "null"){
	                                user3Count++;
	                                try {
	                                	// Create a separate array to calculate median and averages
										user3FileList[user3Count - 1] = milliDateConverter(pairList[i][2]);
									} catch (ParseException e) {
										System.out.println("It looks like this file" + pairList[i][0] + "doesn't have a proper creation date... hmmm"); 
									}
	                            }
	                            
	                            else if ( user4.equals(pairList[i][1]) && user4 != "null"){
	                                user4Count++;
	                                try {
	                                	// Create a separate array to calculate median and averages
										user4FileList[user4Count - 1] = milliDateConverter(pairList[i][2]);
									} catch (ParseException e) {
										System.out.println("It looks like this file" + pairList[i][0] + "doesn't have a proper creation date... hmmm"); 
									}
	                            }
	                            
	                            else if ( user5.equals(pairList[i][1]) && user5 != "null"){
	                                user5Count++;
	                                try {
	                                	// Create a separate array to calculate median and averages
										user5FileList[user5Count - 1] = milliDateConverter(pairList[i][2]);
									} catch (ParseException e) {
										System.out.println("It looks like this file" + pairList[i][0] + "doesn't have a proper creation date... hmmm"); 
									}
	                            }
	                            
	                            else if ( user6.equals(pairList[i][1]) && user6 != "null"){
	                                user6Count++;
	                                try {
	                                	// Create a separate array to calculate median and averages
										user6FileList[user6Count - 1] = milliDateConverter(pairList[i][2]);
									} catch (ParseException e) {
										System.out.println("It looks like this file" + pairList[i][0] + "doesn't have a proper creation date... hmmm"); 
									}
	                            }
	                        }
	                        
//*****************************************************************************************************	                        
//	                        FEATURE ADDITION FOR AVERAGE / MEDIAN COMPLETION TIME
//****************************************************************************************************	 


	                        Arrays.sort(user1FileList);
	                        List<Long> user1SortedList = new ArrayList<Long>();
	                        for (int i=0; i<500; i++){
	                        	if ( !(user1FileList[i] == 0) )
	                        		user1SortedList.add(user1FileList[i]);
	                        }
	      
	                        Arrays.sort(user2FileList);
	                        List<Long> user2SortedList = new ArrayList<Long>();
	                        for (int i=0; i<500; i++){
	                        	if ( !(user2FileList[i] == 0) )
	                        		user2SortedList.add(user2FileList[i]);
	                        }
	                        
	                        Arrays.sort(user3FileList);
	                        List<Long> user3SortedList = new ArrayList<Long>();
	                        for (int i=0; i<500; i++){
	                        	if ( !(user3FileList[i] == 0) )
	                        		user3SortedList.add(user3FileList[i]);
	                        }
	                        
	                        Arrays.sort(user4FileList);
	                        List<Long> user4SortedList = new ArrayList<Long>();
	                        for (int i=0; i<500; i++){
	                        	if ( !(user4FileList[i] == 0) )
	                        		user4SortedList.add(user4FileList[i]);
	                        }
	                        
	                        Arrays.sort(user5FileList);
	                        List<Long> user5SortedList = new ArrayList<Long>();
	                        for (int i=0; i<500; i++){
	                        	if ( !(user5FileList[i] == 0) )
	                        		user5SortedList.add(user5FileList[i]);
	                        }
	                        
	                        Arrays.sort(user6FileList);
	                        List<Long> user6SortedList = new ArrayList<Long>();
	                        for (int i=0; i<500; i++){
	                        	if ( !(user6FileList[i] == 0) )
	                        		user6SortedList.add(user6FileList[i]);
	                        }
	                        
	                        // Array list averages
	                        double user1Average = 0;
	                        double user2Average = 0;
	                        double user3Average = 0;
	                        double user4Average = 0;
	                        double user5Average = 0;
	                        double user6Average = 0;
	                        user1Average = calculateAverage(user1SortedList, searchDate);
	                        user2Average = calculateAverage(user2SortedList, searchDate);
	                        user3Average = calculateAverage(user3SortedList, searchDate);
	                        user4Average = calculateAverage(user4SortedList, searchDate);
	                        user5Average = calculateAverage(user5SortedList, searchDate);
	                        user6Average = calculateAverage(user6SortedList, searchDate);
	                        
	                        
//8888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
// Needs work
//8888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
	                        // Print the counts for each owner
	                        System.out.println();
	                        System.out.println("Reports in " + path);
	                        if (user1Count != 0){
	                            System.out.println(user1 + ": " + user1Count);
	                            String user1Ave = String.format("%d min, %d sec", 
	                            	    TimeUnit.MILLISECONDS.toMinutes( (long) user1Average),
	                            	    TimeUnit.MILLISECONDS.toSeconds( (long) user1Average) - 
	                            	    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes( (long) user1Average))
	                            	);
	                            System.out.println("Average time per report: " + user1Ave);
	                            System.out.println("");
	                        }
	                        if (user2Count != 0){
	                            System.out.println(user2 + ": " + user2Count);
	                            String user2Ave = String.format("%d min, %d sec", 
	                            	    TimeUnit.MILLISECONDS.toMinutes( (long) user2Average),
	                            	    TimeUnit.MILLISECONDS.toSeconds( (long) user2Average) - 
	                            	    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes( (long) user2Average))
	                            	);
	                            System.out.println("Average time per report: " + user2Ave);
	                            System.out.println("");
	                        }
	                        if (user3Count != 0){
	                            System.out.println(user3 + ": " + user3Count);
	                            String user3Ave = String.format("%d min, %d sec", 
	                            	    TimeUnit.MILLISECONDS.toMinutes( (long) user3Average),
	                            	    TimeUnit.MILLISECONDS.toSeconds( (long) user3Average) - 
	                            	    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes( (long) user3Average))
	                            	);
	                            System.out.println("Average time per report: " + user3Ave);
	                            System.out.println("");
	                        }
	                        if (user4Count != 0){
	                            System.out.println(user4 + ": " + user4Count);
	                            String user4Ave = String.format("%d min, %d sec", 
	                            	    TimeUnit.MILLISECONDS.toMinutes( (long) user4Average),
	                            	    TimeUnit.MILLISECONDS.toSeconds( (long) user4Average) - 
	                            	    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes( (long) user4Average))
	                            	);
	                            System.out.println("Average time per report: " + user4Ave);
	                            System.out.println("");
	                        }
	                        if (user5Count != 0){
	                            System.out.println(user5 + ": " + user5Count);
	                            String user5Ave = String.format("%d min, %d sec", 
	                            	    TimeUnit.MILLISECONDS.toMinutes( (long) user5Average),
	                            	    TimeUnit.MILLISECONDS.toSeconds( (long) user5Average) - 
	                            	    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes( (long) user5Average))
	                            	);
	                            System.out.println("Average time per report: " + user5Ave);
	                            System.out.println("");
	                        }
	                        if (user6Count != 0){
	                            System.out.println(user6 + ": " + user6Count);
	                            String user6Ave = String.format("%d min, %d sec", 
	                            	    TimeUnit.MILLISECONDS.toMinutes( (long) user6Average),
	                            	    TimeUnit.MILLISECONDS.toSeconds( (long) user6Average) - 
	                            	    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes( (long) user6Average))
	                            	);
	                            System.out.println("Average time per report: " + user6Ave);
	                            System.out.println("");
	                        }
	                    }
                	}
                }
                break;
                // Exit
                case 5: {
                    System.out.println();
//                    scan.close();
                    System.exit(0);
                }
                break;
            }
        }
    }
    
    public static ArrayList<String> removeDuplicates(ArrayList<String> list) {
    	// Store unique items in result.
    	ArrayList<String> result = new ArrayList<>();
    	// Record encountered Strings in HashSet.
    	HashSet<String> set = new HashSet<>();
    	// Loop over argument list.
    	for (String item : list) {
		    // If String is not in set, add it to the list and the set.
		    if (!set.contains(item) && item != null) {
		    	result.add(item);
		    	set.add(item);
		    }
    	}
    	
	return result;
    }
    
    public static int printMenu(){
    	System.out.println();
    	BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
    	int temp1 = 0;
    	System.out.println("Select what you would like to do.");
    	System.out.println("1. Set File Paths");
    	System.out.println("2. Show Current File Paths");
    	System.out.println("3. Delete a file path");
    	System.out.println("4. Run Counts");
    	System.out.println("5. Exit");

    	try {
			temp1 = Integer.parseInt(input.readLine());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return temp1;
    }

    private static void createPath(){
		File theDir = new File(System.getenv("LOCALAPPDATA") + "\\CottaCapital");
		// if the directory does not exist, create it
		if (!theDir.exists()) {
		    System.out.println("creating directory: CottaCapital");
		    boolean result = false;
		
		    try{
		        theDir.mkdirs();
		        result = true;
		    } 
		    catch(SecurityException se){

		    	System.err.println("Security exception creating file directory.");
		    }        
		    if(result) {    
		        System.out.println("DIR created");  
		    }
		}
		

    }

    private static void writeToFile(int num){
    	try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(System.getenv("LOCALAPPDATA") + "\\CottaCapital\\paths.txt"), "utf-8"))) {
    		for (int i=0; i<10; i++){
            	writer.write(filePath[i] + "\n");
            }
    		if (num == 1)
    			System.out.println("The file path has been added");
    		else
    			System.out.println("The file path has been deleted");
        } catch (IOException e) {
            System.err.println("Problem writing to the file paths.txt");
        }
    }
    
    private static void readConfigs(){
    	String path = System.getenv("LOCALAPPDATA") + "\\CottaCapital\\paths.txt";
        try(BufferedReader br = new BufferedReader(
                new FileReader(path))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            int count = 0;
            while (line != null) {
                filePath[count] = line;
                count++;
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
        }
        catch (IOException io){	
            System.out.println("Something went wrong reading the config file or it doesn't exist.");
            System.out.println("If this is the first time you are seeing this, please disregard.");
        }
    }
    
    private static void listFiles(String directoryName){
        File directory = new File(directoryName);
        int i = 0;
        //get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file : fList){
            if (file.lastModified() >= searchDate && 
                    file.lastModified() <= searchDate+86400000){
                try {
                	pairList[i][0] = file.getAbsolutePath();
                    pairList[i][1] = getOwner(file);
                    pairList[i][2] = getCreationTime(file);
                    i++;
                } catch (IOException ex) {
                    Logger.getLogger(ReportCounts.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        }
    }
    
    private static long dateConverter(String date) throws ParseException {
        String dayDate = date;            
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Date day = new Date();
        try {
            day = sdf.parse(dayDate);
        }
        catch (NullPointerException pe){   
        }
        return day.getTime();
    }
    
    private static long milliDateConverter(String date) throws ParseException {
        String dayDate = date;            
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Date day = new Date();
        try {
            day = sdf.parse(dayDate);
        }
        catch (NullPointerException pe){   
        }
        return day.getTime();
    }
    
    private static String getCreationTime(File f) throws ParseException {
    	Path p = Paths.get(f.getAbsolutePath());
    	BasicFileAttributes attr;
    	String dateCreated = "";
		try {
			attr = Files.readAttributes(p, BasicFileAttributes.class);
			FileTime date = attr.creationTime();
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			String dateCreated1 = df.format(date.toMillis());
			dateCreated = dateCreated1;

		} catch (IOException e) {
			System.out.println("Could not get last modified time.");
		}
    	return dateCreated;
    }

    private static String getOwner(File f) throws IOException {
    Path p = Paths.get(f.getAbsolutePath());
    UserPrincipal owner = Files.getOwner(p);
    return owner.getName();
    }
    
    private static double calculateAverage(List <Long> times, long searchDate) {
    	  Long sum = 0l;
    	  if(!times.isEmpty()) {
    	    for (Long time : times) {
    	        sum += (time - times.get(0));
    	    }
    	    // subtract half hour for lunch below. remove when implemented
    	    return sum.doubleValue() / times.size();
    	  }
    	  return sum;
    	}
    
    @Override
    public String name() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public UserPrincipal getOwner() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public void setOwner(UserPrincipal owner) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    
}